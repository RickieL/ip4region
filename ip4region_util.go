package ip4region

import (
	"strings"
)

func (ip IPInfo) String() string {
	return ip.Country + "|" + ip.Region + "|" + ip.Province + "|" + ip.City + "|" + ip.ISP
}

func getIPInfo(line []byte) IPInfo {

	lineSlice := strings.Split(string(line), "|")
	ipInfo := IPInfo{}
	length := len(lineSlice)
	//ipInfo.CityId = cityId
	if length < 5 {
		for i := 0; i <= 5-length; i++ {
			lineSlice = append(lineSlice, "")
		}
	}

	ipInfo.Country = lineSlice[0]
	ipInfo.Region = lineSlice[1]
	ipInfo.Province = lineSlice[2]
	ipInfo.City = lineSlice[3]
	ipInfo.ISP = lineSlice[4]
	return ipInfo
}

func getLong(b []byte, offset int64) int64 {

	val := (int64(b[offset])<<24 |
		int64(b[offset+1])<<16 |
		int64(b[offset+2])<<8 |
		int64(b[offset+3]))

	return val
}
func (ip4 *IP4Region) getLong(offset int64) int64 {

	val := (int64(ip4.dbBinStr[offset])<<24 |
		int64(ip4.dbBinStr[offset+1])<<16 |
		int64(ip4.dbBinStr[offset+2])<<8 |
		int64(ip4.dbBinStr[offset+3]))

	return val
}

func getLongLitte(b []byte, offset int64) int64 {

	val := (int64(b[offset]) |
		int64(b[offset+1])<<8 |
		int64(b[offset+2])<<16 |
		int64(b[offset+3])<<24)

	return val
}

func (ip4 *IP4Region) getLongLitte(offset int64) int64 {

	val := (int64(ip4.dbBinStr[offset]) |
		int64(ip4.dbBinStr[offset+1])<<8 |
		int64(ip4.dbBinStr[offset+2])<<16 |
		int64(ip4.dbBinStr[offset+3])<<24)

	return val
}

# ip4region

## 介绍

ip4region 的golang module.
可以通过 `import gitee.com/RickieL/ip4region` 来使用

## 使用说明

```go
package main

import (
    "fmt"
    "os"

    "gitee.com/RickieL/ip4region"
)

func main() {

    db := os.Args[1]

    _, err := os.Stat(db)
    if os.IsNotExist(err) {
        panic("not found db " + db)
    }

    region, err := ip4region.New(db)
    defer region.Close()

    ip, err := region.BinarySearch(os.Args[2])

    fmt.Printf("ip: %v, \nerr: %v\n", ip, err)
}
```  

```shell
bash > go run main.go ../ip4regionMaker/ip4region.db 1.0.3.1 

ip: 中国|0|福建省|福州市|电信, 
err: <nil>

```
